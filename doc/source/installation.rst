============
Installation
============

Requirements
------------

- Python >= 3.4
- Almanach >= 4.0.9

Instructions
------------

We strongly encourage tu use a virtual environment to install the software: :code:`python3 -m venv my_virtual_env`.

At the command line::

    $ pip install python-almanachclient

