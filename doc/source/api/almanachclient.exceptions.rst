The :mod:`almanachclient.exceptions` Module
===========================================

.. automodule:: almanachclient.exceptions
  :members:
  :undoc-members:
  :show-inheritance:
