The :mod:`almanachclient.shell` Module
======================================

.. automodule:: almanachclient.shell
  :members:
  :undoc-members:
  :show-inheritance:
