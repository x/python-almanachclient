.. toctree::
   :maxdepth: 1

   almanachclient.exceptions.rst
   almanachclient.keystone_client.rst
   almanachclient.v1.client.rst
