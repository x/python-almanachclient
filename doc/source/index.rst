Almanach API Client
===================

Contents:

.. toctree::
   :maxdepth: 2

   readme
   installation
   usage_cli
   usage_api
   contributing
